var _width = 70;
var _height = 70;
var _margin = 10;
var _padding = 10;

var _grid = []

var initGrid = function() {
	_grid.push([{"text": "Double-click to add text", "class": "box"}, {"text": "+", "class": "box add-row"}], [{"text": "+", "class": "box add-column"}]);
}

var addListeners = function() {
	$(".add-column").on("click", function() {
		addColumn();
	});
	$(".add-row").on("click", function() {
		addRow($(this).parent().index());
	});
	$(".box p").on("dblclick", function() {
		addText(this);
	})
}

var drawGrid = function() {
	$("#grid").empty();
	for (var i = 0; i < _grid.length; i++) {
		
		var new_column = $( document.createElement('div'));
		new_column.attr("class", "column");
		new_column.attr("id", i);

		//If we're the last column, then we draw the plus
		for (var j = 0; j < _grid[i].length; j++) {
			new_column.append("<div class='" + _grid[i][j].class + "' name='" + j + "'><p>" + _grid[i][j].text + "</p></div>");
		}
		$('#grid').append(new_column);
	}
	addListeners();
}

var addColumn = function() {
	var new_box = [{"text": "Double-click to add text", "class": "box"}, {"text": "+", "class": "box add-row"}];
	_grid.splice(_grid.length - 1, 0, new_box);
	drawGrid();
}

var addRow = function(column) {
	var new_box = {"text": "Double-click to add text", "class": "box"};
	_grid[column].splice(_grid[column].length -1, 0, new_box);
	drawGrid();
}

var addText = function(p) {
	var box = $(p).parent();
	var boxIndex = parseInt($(box).attr("name"));
	var columnIndex = $(box).parent().index();
	console.log(columnIndex);
	$(box).html("<input type='text' class='text-input'/>");
	$('.text-input').focus();
	$('.text-input').focusout(function() {
		var text = $('.text-input').val();
		if (text.length === 0 || text === "") {
			$(box).html("<p>Add a title</p>");
		} else {
			_grid[columnIndex][boxIndex].text = text;
		}
		drawGrid();
	});
	addListeners();
}

$(function() {
	initGrid();
	drawGrid();
});